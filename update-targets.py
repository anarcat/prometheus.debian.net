#!/usr/bin/python3

import logging
import os
import re
import subprocess
import tempfile


TEST_LDAP_ENTRIES = """# extended LDIF
#
# LDAPv3
# base <dc=debian,dc=org> with scope subtree
# filter: (dnsZoneEntry=*)
# requesting: dnsZoneEntry
#

# kk, users, debian.org
dn: uid=kk,ou=users,dc=debian,dc=org
ufn: kk, users, debian.org
dnsZoneEntry: kk IN CNAME hagrid.dyn.ee.

# rb, users, debian.org
dn: uid=rb,ou=users,dc=debian,dc=org
ufn: rb, users, debian.org
dnsZoneEntry: minnesota IN A 64.211.114.35
dnsZoneEntry: minnesota IN MX 0 minnesota.debian.net.

# ed, users, debian.org
dn: uid=ed,ou=users,dc=debian,dc=org
ufn: ed, users, debian.org
dnsZoneEntry: boraas IN CNAME cobalt.debian.net.
dnsZoneEntry: cobalt IN A 24.86.204.39
dnsZoneEntry: kronos IN CNAME cobalt.debian.net.
dnsZoneEntry: kronos-x IN CNAME cobalt.debian.net.
"""


def parse_ldap_entries(output: str) -> list[str]:
    r = re.compile(r"dnsZoneEntry: ([^ ]+) IN +(A|AAAA|CNAME) +.*$", re.MULTILINE)
    for entry in r.finditer(output):
        name = entry.group(1)
        if "_domainkey" in name:
            continue
        if name.startswith("_dmarc"):
            continue
        if name.startswith("_"):
            continue
        yield entry.group(1)


def test_parse_ldap_entries():
    assert sorted(list(set(parse_ldap_entries(TEST_LDAP_ENTRIES)))) == [
        'boraas',
        'cobalt',
        'kk',
        'kronos',
        'kronos-x',
        'minnesota',
    ]


def main():
    # equivalent to
    # ldapsearch -u -x -H ldap://db.debian.org -b dc=debian,dc=org '(dnsZoneEntry=*)' dnsZoneEntry | \
    #   grep ^dnsZoneEntry | \
    #   grep -e ' A ' -e ' AAAA ' -e ' CNAME ' | \
    #   sed -s 's/dnsZoneEntry: //;s/ .*/.debian.net/' | \
    #   sort -u
    output = subprocess.check_output([
        'ldapsearch', '-u', '-x', '-H', 'ldap://db.debian.org', '-b', 'dc=debian,dc=org', '(dnsZoneEntry=*)', 'dnsZoneEntry'
    ], encoding='utf-8')


    blackbox_contents = "- targets:\n"

    for host in sorted(list(set(parse_ldap_entries(output)))):
        blackbox_contents += "  - %s.debian.net\n" % host

    os.makedirs("/etc/prometheus/targets.d", exist_ok=True)
    with open("/etc/prometheus/targets.d/blackbox_icmp_hosts.yaml") as current:
        if current.read() == blackbox_contents:
            return

        with tempfile.NamedTemporaryFile(
                prefix="/etc/prometheus/targets.d/blackbox_icmp_hosts.yaml.",
                mode="w+",
                encoding="utf-8",
                delete=False
        ) as fp:
            fp.write(blackbox_contents)
            fp.flush()

        try:
            subprocess.call(["diff", "-u", "/etc/prometheus/targets.d/blackbox_icmp_hosts.yaml", fp.name])
        except subprocess.CalledProcessError as e:
            logging.warning("failed to call diff: %s", e)

        try:
            os.chmod(fp.name, 0o644)
            os.rename(fp.name, "/etc/prometheus/targets.d/blackbox_icmp_hosts.yaml")
        except OSError as e:
            logging.error("failed to deploy new configuration file: %s", e)
            os.unlink(fp.name)
        subprocess.check_call(["service", "prometheus", "restart"])




if __name__ == '__main__':
    main()
