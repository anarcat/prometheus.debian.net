#!/bin/sh

set -e

ldapsearch -u -x -H ldap://db.debian.org -b dc=debian,dc=org '(dnsZoneEntry=*)' dnsZoneEntry | grep ^dnsZoneEntry | grep -e ' A ' -e ' AAAA ' -e ' CNAME ' | sed -s 's/dnsZoneEntry: //;s/ .*/.debian.net/' | sort -u
#ldapsearch -u -x -H ldap://db.debian.org -b dc=debian,dc=org \"(dnsZoneEntry=*)\" dnsZoneEntry | grep ^dnsZoneEntry | grep -e \" A \" -e \" AAAA \" -e \" CNAME \" | sed -s \"s/dnsZoneEntry: //;s/ .*/.debian.net/\" | sort -u
