# prometheus.debian.net configuration

This repository holds the configuration for the
`prometheus.debian.net` host which is tasked with monitoring the
various services hosted under the [`debian.net` domain](https://wiki.debian.org/Teams/DebianNet).

It is the content of the `/etc/prometheus` directory on the server,
literally. There are some additional files to automate some of the
content creation, but nothing completed to satisfaction:

 * `update-targets.py`: hack-ish Python script to scrape the list of
   hosts under `debian.net` from LDAP
 * `ansible/`: an [Ansible](https://www.ansible.com/) playbook that was partly used for the
   original setup and is in development. the `update-targets.py` stuff
   is *not* managed yet

## Future work

 * scrape additional metrics from targets interested in such
   functionality, probably through the [team infra repository](https://salsa.debian.org/debian.net-team/infra/)

 * scrape HTTP / HTTPS, including cert verification and performance
   metrics

 * automate pulling configuration, possibly from the infra repository,
   or this very repository

 * decide between using this git repository as the source for
   `/etc/prometheus` on the host, or as a source for Ansible (and then
   removing the `/etc/prometheus` rendered file from this
   repository) - update: probably going to use Ansible for everything
   here

## Credits

This was partly inspired by the [video team Ansible repository](https://salsa.debian.org/debconf-video-team/ansible),
particularly the [prometheus/server role](https://salsa.debian.org/debconf-video-team/ansible/-/tree/master/roles/prometheus/server), which we could still use
here.

## Other resources

This service is also documented in the following pages:

 * [DebianNetDomains](https://wiki.debian.org/DebianNetDomains)
 * [Service/Prometheus](https://wiki.debian.org/Services/Prometheus)
 * [Creation request issue](https://salsa.debian.org/debian.net-team/requests/-/issues/17)
 * [Infrastructure repo](https://salsa.debian.org/debian.net-team/infra)
